{ alsaLib, autoPatchelfHook, fetchzip, gnome2, gtk2, gtk3, nodejs, nss, python2, stdenv, udev, unzip, wrapGAppsHook, xorg, yarn, ... }:

{
  buildInputs = [
    nodejs
    python2
    xorg.xorgserver
    yarn
    (stdenv.mkDerivation rec {
      dontPatchELF = true;
      installPhase = ''
        mkdir -p $out/bin $out/opt/cypress
        cp -vr * $out/opt/cypress/
        echo '{"verified": true}' > $out/opt/cypress/binary_state.json
        echo "echo $out/opt/cypress/" > $out/opt/cypress/locate_cypress.sh
        chmod +x $out/opt/cypress/locate_cypress.sh
        ln -s $out/opt/cypress/Cypress $out/bin/Cypress
        ln -s $out/opt/cypress/locate_cypress.sh $out/bin/locate_cypress.sh
      '';
      pname = "cypress";
      version = "3.4.1";
      buildInputs = with xorg; [ libXScrnSaver libXdamage libXtst ] ++ [ nss gtk2 alsaLib gnome2.GConf gtk3 unzip ];
      nativeBuildInputs = [ autoPatchelfHook wrapGAppsHook ];
      runtimeDependencies = [ udev.lib ];
      meta = with stdenv.lib; {
        description = "Fast, easy and reliable testing for anything that runs in a browser.";
        homepage = "https://www.cypress.io/";
        license = licenses.mit;
        platforms = ["x86_64-linux"];
        maintainers = with maintainers; [ tweber mmahut ];
      };
      src = fetchzip {
        sha256 = "1gyl5c86gr5sv6z5rkg0afdxqrmsxmyrimm1p5q6jlrlyzki1bfs";
        url = "https://cdn.cypress.io/desktop/${version}/linux-x64/cypress.zip";
      };
    })
  ];
}
