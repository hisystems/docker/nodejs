# CHANGELOG

<!--- next entry here -->

## 1.0.1
2020-01-28

### Fixes

- **ci:** Fixes build job. (1e28d0a33e7f27b38657a77beac2d05908eca41e)
- **ci:** Fixes git tag fetching. (ddbd03c9982f27bf386390df50de4106ba47514a)
- **ci:** Fixes build job. (da9b42a74b1e7a0fd273fb8a2a9d7ae8fea67985)

## 1.0.0
2020-01-28

### Features

- **ci:** Initial Commit. (08ae2a5a9daa972ede40e0b3d460096ec26a677b)

### Fixes

- **ci:** Fixes mirror stage. (8b09be6241b94da880e4de8c2f254b2ab77fb257)